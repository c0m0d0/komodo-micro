# Dispositivos Komodo

La configuración interna del Arduino es definida de acuerdo a los parámetros especificados dentro del archivo. Ino en las siguientes líneas:
```javascript

// Inicia la librería de servidor Ethernet
// Con la dirección IP y el puerto especificado
// (El puerto 80 es por defecto en HTTP):
EthernetServer server(80);
// Ingrese dirección MAC del dispositivo y la ip dentro de la cual operabyte 
mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,166);

// Ingrese la dirección ip del sevidor del sistema Komodo
IPAddress serverAddr(186,58,1562,12);

Nótese que los comentarios incluidos dentro del código especifican la definición de cada variable para facilitar el acceso al usuario.
```