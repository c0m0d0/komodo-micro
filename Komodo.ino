/*
Web Server

A simple web server that shows the value of the analog input pins.
using an Arduino Wiznet Ethernet shield. 

Circuit:
* Ethernet shield attached to pins 10, 11, 12, 13
* Analog inputs attached to pins A0 through A5 (optional)

created 18 Dec 2009
by David A. Mellis
modified 9 Apr 2012
by Tom Igoe
Adapted to Komodo System 10/1/18
by Javier Vallenilla
*/

#include <SPI.h>
#include <UIPEthernet.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <MultiTaskLib.h>


struct Params {
  int pingLoop;
  IPAddress ip[7];
  boolean ipStatus[7];
};


// Inicia la librería de servidor Ethernet
// Con la dirección IP y el puerto especificado
// (El puerto 80 es por defecto en HTTP):
EthernetServer server(80);
// Ingrese dirección MAC del dispositivo y la ip dentro de la cual operabyte 
mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,166);

// Ingrese la dirección ipo del sevidor del sistema Komodo
IPAddress serverAddr(186,58,1562,12);


Params startParams;
int eeAddress = 0;
MultiTask multitask(2);
EthernetClient client;

void(* resetFunc) (void) = 0; //reinca arduino

void writeOk(){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html");
  client.println();
  
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<p>Hello from Komodo!<P/><br/>");
  
  client.println("</html>");
  
  client.println();
}

void writeErr(IPAddress ipPing){
  client.connect(serverAddr, 80);
  String PostData = "Error From Komodo!\n Error en la dirección IP:";
  PostData += ipPing;
  PostData += "\n Se ha enviado un reporte de error";
  
  client.println("POST / HTTP/1.1");
  client.println("Host: **.**.**.**");
  client.println("Connection: close\r\nContent-Type: application/x-www-form-urlencoded");
  client.print("Content-Length: ");
  client.println(PostData.length());
  client.println();
  client.println(PostData);
}

boolean makePing(IPAddress ipPing){
  client.setTimeout(2000);
  if (!client.connect(ipPing, 80)) {
    writeErr(ipPing);
    return false;
  }else{
    writeOk();
    return true;
  }
}

IPAddress trimIp(String ip){
  String ipSegment;
  int indexSegment[4];
  byte newIp[4];
  
  indexSegment[0] = ip.indexOf('.');  //finds location of first .
  ipSegment = ip.substring(0, indexSegment[0]);//captures first data String
  newIp[0] = ipSegment.toInt();
  
  indexSegment[1] = ip.indexOf('.', indexSegment[0]+1 );   //finds location of second ,
  ipSegment = ip.substring(indexSegment[0]+1, indexSegment[1]+1);   //captures second data String
  newIp[1] = ipSegment.toInt();
  
  indexSegment[2] = ip.indexOf('.', indexSegment[1]+1 );
  ipSegment = ip.substring(indexSegment[1]+1, indexSegment[2]+1);
  newIp[2] = ipSegment.toInt();
  
  indexSegment[3] = ip.indexOf('.', indexSegment[2]+1 );
  ipSegment = ip.substring(indexSegment[3]+1); //captures remain part of data after last ,
  newIp[3] = ipSegment.toInt();
  IPAddress formedIP(newIp);
  return formedIP;
}

void reConf() {
  
}

void setup(){ // Inicia la comunicación serial y espera por el puerto para conexión:
  Serial.begin(9600);
  while (!Serial){ // Espera para la conexión por le puerto serial. Needed for Leonardo only
  }
  
  // Inicia la conexión Ethernet y el servidor
  Ethernet.begin(mac, ip);
  
  
  // Establece los pines de salida para las interfaces
  pinMode(3, OUTPUT); // interfaz 1
  pinMode(4, OUTPUT); // interfaz 2
  pinMode(5, OUTPUT); // interfaz 3
  pinMode(6, OUTPUT); // interfaz 4
  pinMode(7, OUTPUT); // interfaz 5
  pinMode(8, OUTPUT); // interfaz 6
  pinMode(9, OUTPUT); // interfaz 7
  
  
  EEPROM.get( eeAddress, startParams );
  
  multitask.AddContinuous(100,
    [](){
      server.begin();
      
      Serial.print("Servidor iniciado en ");
      Serial.println(Ethernet.localIP());
      
      client = server.available();
      if (client) { Serial.println("new client");
      // Si el request http comienza con una linea vacia
      boolean currentLineIsBlank = true;
      while (client.connected()){ 
        if (client.available()){
          char c = client.read();
          Serial.write(c);
          if (c == '\n' && currentLineIsBlank){ 
            // Envia una respuerta http estándar
            //   String requestBody;
            
            while (client.available()) {
              const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
              DynamicJsonBuffer jsonBuffer(capacity);
              
              JsonObject& root = jsonBuffer.parseObject(client);
              if (!root.success()) {
                Serial.println(F("Parsing failed!"));
                return;
              }
              
              // Extrae los valores
              int interface = root["interface"];
              String status = root["status"];
              String config = root["config"];
              String ipPing = root["ping"];
              
              if(config.equals("true")){
                //Lee los parámetro de la nueva configuración
                Params newParams = {
                  root["lopp-time"],{
                    trimIp(root["params"]["interface-1"]["ip"]),
                    trimIp(root["params"]["interface-2"]["ip"]),
                    trimIp(root["params"]["interface-3"]["ip"]),
                    trimIp(root["params"]["interface-4"]["ip"]),
                    trimIp(root["params"]["interface-5"]["ip"]),
                    trimIp(root["params"]["interface-6"]["ip"]),
                    trimIp(root["params"]["interface-7"]["ip"])
                  },{
                    root["params"]["interface-1"]["status"],
                    root["params"]["interface-2"]["status"],
                    root["params"]["interface-3"]["status"],
                    root["params"]["interface-4"]["status"],
                    root["params"]["interface-5"]["status"],
                    root["params"]["interface-6"]["status"],
                    root["params"]["interface-7"]["status"]
                  }
                };
                //escribe los nuevos parámetros de configuración y reinicia el Arduino
                EEPROM.put(eeAddress, newParams);
                delay(1000);
                resetFunc();
                return;
              }
              
              if(ipPing.equals("true")){
                //lee una nueva peticion de ping 
                if(!makePing(trimIp(root["ipPing"]))){
                  return;
                }
              }
              
              if(status.equals("on")){
                //lee una peticion de reincio, encendido o apagado
                digitalWrite(interface, HIGH);  //encendido
              }else if(status.equals("off")){
                digitalWrite(interface, LOW);   //apagado
              }else if(status.equals("reboot")){
                digitalWrite(interface, LOW);   //reinicio
                delay(1000);
                digitalWrite(interface, HIGH);
              }        
            }
            writeOk();
            break;
          }
          if (c == '\n'){ 
            currentLineIsBlank = true;
          } 
          else if (c != '\r'){ 
            currentLineIsBlank = false;
          }
        }
      }
      // otorga tiempo el cliente para procesar la respuesta
      delay(1);
      // cierra la conexión
      client.stop();
      Serial.println("cliente desconectado");
    } 
    
  }
);

multitask.AddContinuous(startParams.pingLoop,
  []() {
    for(int i=0; i<7;i++){
      if(startParams.ipStatus[i]){
        makePing(startParams.ip[i]);
      }
    }
  }
);

}

void loop(){ // Espera por clientes entrantes
  Serial.println("Main Loop");
  while (1)
  {
    multitask.Update();
  }
}


